from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
def index(request):
    # return HttpResponse("Hola, mundo")
    return render(request, 'app/index.html')


def profesor(request):
    return render(request,'app/profesor.html')
    
def admin(request):
    return render(request,'app/admin.html')

def estudiante(request):
    return render(request,'app/estudiante.html')